package toolbox

type SocketMessage struct {
	MessageType int         `json:"type"`
	Code        int         `json:"code"`
	Data        interface{} `json:"data"`
}

//goland:noinspection GoUnusedExportedFunction
func CreateSocketError(err Error) SocketMessage {
	return SocketMessage{
		MessageType: 0,
		Code:        err.Code,
		Data:        err.Error(),
	}
}

//goland:noinspection GoUnusedExportedFunction
func CreateSocketMessage(messageType int, data interface{}) SocketMessage {
	return SocketMessage{
		MessageType: messageType,
		Data:        data,
	}
}
