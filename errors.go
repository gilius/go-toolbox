package toolbox

import "net/http"

type Error struct {
	Code    int
	Message string
}

func (e *Error) Error() string {
	return e.Message
}

//goland:noinspection GoUnusedExportedFunction
func ErrorNotFound(err string) *Error {
	return &Error{
		Code:    http.StatusNotFound,
		Message: err,
	}
}

//goland:noinspection GoUnusedExportedFunction
func ErrorForbidden(err string) *Error {
	return &Error{
		Code:    http.StatusForbidden,
		Message: err,
	}
}

//goland:noinspection GoUnusedExportedFunction
func ErrorUnauthorized(err string) *Error {
	return &Error{
		Code:    http.StatusUnauthorized,
		Message: err,
	}
}

//goland:noinspection GoUnusedExportedFunction
func ErrorBadRequest(err string) *Error {
	return &Error{
		Code:    http.StatusBadRequest,
		Message: err,
	}
}

//goland:noinspection GoUnusedExportedFunction
func ErrorInternal(err string) *Error {
	return &Error{
		Code:    http.StatusInternalServerError,
		Message: err,
	}
}

//goland:noinspection GoUnusedExportedFunction
func HandleHttpError(err *Error) {
	if err == nil {
		return
	}

	HttpError(*err)
}
