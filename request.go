package toolbox

import (
	"bytes"
	"encoding/json"
	"errors"
	"github.com/gorilla/websocket"
	"log"
	"net/http"
	"regexp"
	"runtime/debug"
	"sync"
	"time"
)

const ErrFailedToParseRequestData = "invalid_request_data"
const ErrUnknown = "unknown"

//goland:noinspection GoUnusedExportedFunction
func JsonBody(r *http.Request, item interface{}) {
	err := json.NewDecoder(r.Body).Decode(&item)
	if err != nil {
		HttpBadRequest(errors.New(ErrFailedToParseRequestData))
	}
}

//goland:noinspection GoUnusedExportedFunction
func HandleRequest(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.Header.Get("Upgrade") != "websocket" {
			defer writeResponse(w)
		}
		next.ServeHTTP(w, r)
	})
}

var upgrader *websocket.Upgrader

func getUpgrader() *websocket.Upgrader {
	if upgrader == nil {
		upgrader = &websocket.Upgrader{
			CheckOrigin: func(r *http.Request) bool {
				return true //Allow CORS
			},
		}
	}

	return upgrader
}

func keepAlive(c *websocket.Conn, timeout time.Duration) *sync.Mutex {
	lastResponse := time.Now()
	writeLock := sync.Mutex{}
	c.SetPongHandler(func(msg string) error {
		lastResponse = time.Now()
		return nil
	})

	go func() {
		for {
			err := func() error {
				writeLock.Lock()
				defer writeLock.Unlock()
				return c.WriteMessage(websocket.PingMessage, nil)
			}()
			if err != nil {
				return
			}
			time.Sleep(timeout / 2)
			if time.Since(lastResponse) > timeout {
				_ = c.Close()
				return
			}
		}
	}()

	return &writeLock
}

//goland:noinspection GoUnusedExportedFunction
func BearerAuthRequest(handler func(http.ResponseWriter, *http.Request, string)) func(http.ResponseWriter, *http.Request) {
	return func(writer http.ResponseWriter, request *http.Request) {

		bearer := ""
		for _, cookie := range request.Cookies() {
			if cookie.Name == "jwt" {
				bearer = cookie.Value
				break
			}
		}

		if bearer == "" {
			//Parsing bearer
			authHeader := request.Header.Get("Authorization")
			authRegExp := regexp.MustCompile("^Bearer (.*)$")
			bearer = authRegExp.ReplaceAllString(authHeader, "$1")
		}

		//Running handler with bearer token added
		handler(writer, request, bearer)
	}
}

//goland:noinspection GoUnusedExportedFunction
func EmptySocketReader(ws *websocket.Conn) {
	go func() {
		for {
			_, _, err := ws.ReadMessage()
			if err != nil {
				break
			}
		}
	}()
}

//goland:noinspection GoUnusedExportedFunction
func SocketConnectionBearerRequest(handler func(*websocket.Conn, *http.Request, *sync.Mutex, string)) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, request *http.Request) {
		bearer := ""
		for _, cookie := range request.Cookies() {
			if cookie.Name == "jwt" {
				bearer = cookie.Value
				break
			}
		}

		if bearer == "" {
			//Parsing bearer
			authHeader := request.Header.Get("Authorization")
			authRegExp := regexp.MustCompile("^Bearer (.*)$")
			bearer = authRegExp.ReplaceAllString(authHeader, "$1")
		}

		ws, errConn := getUpgrader().Upgrade(w, request, nil)
		if errConn != nil {
			log.Println(errConn)
			return
		}

		defer func() {
			err := recover()
			if err != nil {
				log.Println(err)
			}
			_ = ws.Close()
		}()

		writeLock := keepAlive(ws, time.Second*30)
		handler(ws, request, writeLock, bearer)
	}
}

//goland:noinspection GoUnusedExportedFunction
func SocketConnectionRequest(handler func(*websocket.Conn, *http.Request, *sync.Mutex)) func(http.ResponseWriter, *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		ws, errConn := getUpgrader().Upgrade(w, r, nil)
		if errConn != nil {
			log.Println(errConn)
			return
		}

		defer func() {
			err := recover()
			if err != nil {
				log.Println(err)
			}
			_ = ws.Close()
		}()
		writeLock := keepAlive(ws, time.Second*30)

		handler(ws, r, writeLock)
	}
}

func writeResponse(w http.ResponseWriter) {

	//Recovering thrown Data
	panicData := recover()
	//If no panic data
	if panicData == nil {
		return
	}

	w.Header().Set("Content-Type", "application/json")

	//Parsing recover() Data into ResponseObject
	var ok bool
	var responseObject ResponseObject
	responseObject, ok = panicData.(ResponseObject)
	if !ok {
		log.Println("Cannot parse panic Data:", panicData, string(debug.Stack()))
		responseObject = ResponseObject{
			Status:  http.StatusInternalServerError,
			Message: ErrUnknown,
		}
	}

	responseObject.Timestamp = time.Now()

	//Parsing response body as JSON
	responseText, err := json.Marshal(responseObject)
	if err != nil {
		log.Println("Error encoding Response Data")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	//Writing response string
	w.WriteHeader(responseObject.Status)
	_, _ = w.Write(responseText)
}

//goland:noinspection GoUnusedExportedFunction
func JsonDecode(response *http.Response, data interface{}) error {
	var item *ResponseObject

	defer func() {
		_ = response.Body.Close()
	}()
	err := json.NewDecoder(response.Body).Decode(&item)
	if err != nil {
		return err
	}

	if item.Data != nil {
		encoded, err := json.Marshal(item.Data)
		if err != nil {
			return err
		}

		err = json.NewDecoder(bytes.NewReader(encoded)).Decode(&data)
		if err != nil {
			return err
		}
	}
	return nil
}
