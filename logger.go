package toolbox

import (
	"log"
	"os"
)

//goland:noinspection GoUnusedExportedFunction
func StdLogger() *log.Logger {
	return log.New(os.Stdout, "SERVICE:", log.Ldate|log.Ltime|log.Lshortfile)
}
