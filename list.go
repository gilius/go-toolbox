package toolbox

type ListResponse struct {
	Items    interface{} `json:"items"`
	Filtered int         `json:"filtered"`
	Total    int         `json:"total"`
}
