package toolbox

import (
	"errors"
	"net/http"
	"time"
)

type ResponseObject struct {
	Status    int         `json:"status"`
	Data      interface{} `json:"data"`
	Timestamp time.Time   `json:"timestamp"`
	Message   string      `json:"message"`
}

//goland:noinspection GoUnusedExportedFunction
func ResponseFromError(error Error) ResponseObject {
	return ResponseObject{
		Status:    error.Code,
		Data:      nil,
		Timestamp: time.Now(),
		Message:   error.Message,
	}
}

func SendResponse(code int, err error, data interface{}) {
	response := ResponseObject{Status: code, Data: data}
	if err != nil {
		response.Message = err.Error()
	}
	panic(response)
}

func HttpError(err Error) {
	SendResponse(err.Code, errors.New(err.Message), nil)
}

//goland:noinspection GoUnusedExportedFunction
func HttpInternal(err error) {
	SendResponse(http.StatusInternalServerError, err, nil)
}

//goland:noinspection GoUnusedExportedFunction
func HttpNotFound(err error) {
	SendResponse(http.StatusNotFound, err, nil)
}

func HttpBadRequest(err error) {
	SendResponse(http.StatusBadRequest, err, nil)
}

//goland:noinspection GoUnusedExportedFunction
func HttpDeleted() {
	SendResponse(http.StatusNoContent, nil, nil)
}

//goland:noinspection GoUnusedExportedFunction
func HttpSuccess(data interface{}) {
	SendResponse(http.StatusOK, nil, data)
}

//goland:noinspection GoUnusedExportedFunction
func HttpCreated(data interface{}) {
	SendResponse(http.StatusCreated, nil, data)
}
